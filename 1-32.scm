#lang racket
(require (planet neil/sicp))

;;; Recursive ;;;

(define (accumulate combiner null-value term a next b)
  (if (> a b)
      null-value
      (combiner (term a)
                (accumulate combiner null-value term (next a) next b))))

(define (sum f a b next)
  (accumulate + 0 f a next b))

(define (product f a b next)
  (accumulate * 1 f a next b))

(define (cube x) (* x x x))
(define (double x) (* 2 x))

(sum cube 1 4 inc) ;; 100
(product double 2 4 inc) ;; 192

;;; Iterative ;;;

(define (product-iterative f a b next)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (* result (f a)))))
  (iter a 1))

(define (accumulate-iter combiner null-value term a next b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (combiner result (term a)))))
  (iter a null-value))

(define (sum-iter f a b next)
  (accumulate + 0 f a next b))

(define (product-iter f a b next)
  (accumulate-iter * 1 f a next b))

(sum-iter cube 1 4 inc) ;; 100
(product-iter double 2 4 inc) ;; 192
