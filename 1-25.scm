#lang racket
(require (planet neil/sicp))

Alyssa's method computes a big-ass exponent first, and then takes the remainder,
whereas the expmod procedure takes the remainder on each iteration, keeping the
number small. Her method would fail pretty quickly, depending on the integer
implementation of her Scheme.
