#lang racket
(require (planet neil/sicp))

Interval arithmetic doesn't obey the laws of algebra we take for granted in the
conversion between the two forms. I would guess the task of designing a system
without this flaw is an unsolved problem, considering the warning.
