#lang racket
(require (planet neil/sicp))

(define (gcd a b)
  (if (= b 0)
      a
      (gcd b (remainder a b))))

;; Normal evaluation
(gcd 206 40)

(gcd 40 (remainder 206 40)

(gcd (remainder 206 40) (remainder 40 (remainder 206 40)))

(gcd (remainder 40 (remainder 206 40)) (remainder (remainder 206 40) (remainder 40 (remainder 206 40))))

(gcd (remainder (remainder 206 40) (remainder 40 (remainder 206 40)))
     (remainder (remainder 40 (remainder 206 40))
                (remainder (remainder 206 40) (remainder 40 (remainder 206 40)))))

Note the fibonacci pattern. In the final step, there are 5 with a=206, 3 with
a=40, 2 with a=6, and 1 with a=2. So with n reductions in the algorithm (4 in
this case), the total number of remainder operations looks to be the sum of the
first n fibonacci numbers (5+3+2+1 = 11 in this case). Actually, if you include
the final reduction where b=0, there is an additional fibonacci addition.

;; Applicative evaluation
(gcd 206 40)
(gcd 40 (remainder 206 40))
(gcd 40 6)
(gcd 6 (remainder 40 6))
(gcd 6 4)
(gcd 4 (remainder 6 4))
(gcd 4 2)
(gcd 2 (remainder 4 2))
(gcd 2 0)
2

n remainder operations are performed, for n steps in the algorithm (4 in this
case).
