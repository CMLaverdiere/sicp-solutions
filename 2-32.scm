#lang racket
(require (planet neil/sicp))

;; The power set that we've seen a million times. To each possible subset, add
;; the head of the list to double the number of sets. This allows each subset to
;; either contain, or not contain each element, giving all possible subsets.

(define (subsets s)
  (if (null? s)
      (list nil)
      (let ((rest (subsets (cdr s))))
        (append rest (map (lambda (r) (cons (car s) r)) rest)))))

(display (subsets '(1 2 3)))
