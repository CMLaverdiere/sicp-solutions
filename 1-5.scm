#lang racket
(require (planet neil/sicp))

;; In applicative-order evaluation, the interpreter will continue expanding (p)
;; to (p), looping forever.

;; With normal-order evaluation, we get

(test 0 (p))
(if (= 0 0)
    0
    (p))

;; and since 0 = 0, we don't need to evaluate (p), so 0 is returned.
