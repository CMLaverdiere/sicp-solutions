#lang racket
(require (planet neil/sicp))

(define (last-elem l)
  (if (null? (cdr l))
      (car l)
      (last-elem (cdr l))))

(define (head l)
  (if (null? (cdr l))
      nil
      (cons (car l) (head (cdr l)))))

(define (reverse l)
  (if (null? l)
      l
      (cons (last-elem l) (reverse (head l)))))

(define (fold-right fn initial seq)
  (if (null? seq) initial
      (fn (car seq) (fold-right fn initial (cdr seq)))))

(define (fold-left fn initial sequence)
  (define (iter result rest)
    (if (null? rest)
        result
        (iter (fn result (car rest))
              (cdr rest))))
  (iter initial sequence))

(define (reverse-right seq)
  (fold-right (lambda (x y) (append y (list x))) nil seq))

(define (reverse-left seq)
  (fold-left (lambda (x y) (cons y x)) nil seq))

(list 1 2 3 4)
(reverse '(1 2 3 4))
(reverse-right '(1 2 3 4))
(reverse-left '(1 2 3 4))
