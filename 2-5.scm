#lang racket
(require (planet neil/sicp))

;; Since 2 and 3 are coprime, any number 2^a * 3^b has a unique prime factorization
;; using just 2 and 3.

(define (divide-out f n)
  (if (= 0 (modulo n f))
      (+ 1 (divide-out f (/ n f)))
      0))

(define (cons a b)
  (* (expt 2 a) (expt 3 b)))

(define (car c)
  (divide-out 2 c))

(define (cdr c)
  (divide-out 3 c))

(car (cons 4 7))
(cdr (cons 4 7))
