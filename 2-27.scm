#lang racket
(require (planet neil/sicp))

(define (last-elem l)
  (if (null? (cdr l))
      (car l)
      (last-elem (cdr l))))

(define (head l)
  (if (null? (cdr l))
      nil
      (cons (car l) (head (cdr l)))))

(define (reverse l)
  (if (null? l)
      l
      (cons (last-elem l) (reverse (head l)))))

(define (deep-reverse l)
  (if (null? l)
      l
      (if (list? l)
          (cons (deep-reverse (last-elem l)) (deep-reverse (head l)))
          l)))

(define x (list (list 1 2) (list 3 4)))
(reverse x)
(deep-reverse x)
