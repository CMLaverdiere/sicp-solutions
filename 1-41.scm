#lang racket
(require (planet neil/sicp))

(define (double f)
  (lambda (x) (f (f x))))

;; ((double inc) x) = (inc (inc x))
;; (double double) = (double (double f))
;; ((double double) inc) = (double (double inc))
;;                       = (double (inc inc))
;;                       = ((inc inc) (inc inc))
;; (double (double double)) inc) = (((double double) (double double)) inc)
;;                               = (inc 16 times)

(((double (double double)) inc) 5) ;; = 21
