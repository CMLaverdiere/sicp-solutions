#lang racket
(require (planet neil/sicp))

(define (make-segment start end)
  (cons start end))

(define (start-segment seg)
  (car seg))

(define (end-segment seg)
  (cdr seg))

(define (make-point x y)
  (cons x y))

(define (x-point point)
  (car point))

(define (y-point point)
  (cdr point))

(define (midpoint-segment seg)
  (average-of-points (start-segment seg) (end-segment seg)))

(define (average-of-points p1 p2)
  (make-point (/ (+ (x-point p1) (x-point p2)) 2)
              (/ (+ (y-point p1) (y-point p2)) 2)))

(define (print-point p)
  (newline)
  (display "(")
  (display (x-point p))
  (display ",")
  (display (y-point p))
  (display ")"))

;; Implementation 1

(define (make-rectangle top-left-corner bottom-right-corner)
  (cons top-left-corner bottom-right-corner))

(define (top-left rect)
  (car rect))

(define (bottom-right rect)
  (cdr rect))

(define (width rect)
  (horiz-length (top-left rect) (bottom-right rect)))

(define (height rect)
  (vert-length (top-left rect) (bottom-right rect)))

(define (horiz-length p1 p2)
  (abs (- (x-point p1) (x-point p2))))

(define (vert-length p1 p2)
  (abs (- (y-point p1) (y-point p2))))

;; Implementation 2

(define (alt-make-rectangle bottom-left width height)
  (cons bottom-left (cons width height)))

(define (alt-width rect)
  (cadr rect))

(define (alt-height rect)
  (cddr rect))

;; General API

(define (perimeter rect)
  (+ (* 2 (width rect))
     (* 2 (height rect))))

(define (area rect)
  (* (width rect) (height rect)))

(define v1 (make-point 1 2))
(define v2 (make-point 4 7))

;; impl 2 redefines
;; (define make-rectangle alt-make-rectangle)
;; (define width alt-width)
;; (define height alt-height)

(define rec (make-rectangle v1 v2)) ;; impl 1
;; (define rec (make-rectangle v1 3 5)) ;; impl 2

(perimeter rec) ;; 3x5 rect => 16 perim
(area rec) ;; 15 area
