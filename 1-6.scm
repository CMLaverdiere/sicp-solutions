#lang racket
(require (planet neil/sicp))

;; Square root

(define (sqrt-iter guess x)
  ;; (new-if (good-enough? guess x)
  (if (good-enough? guess x)
      guess
      (sqrt-iter (improve guess x)
                 x)))

(define (improve guess x)
  (average guess (/ x guess)))

(define (average x y)
  (/ (+ x y) 2))

(define (good-enough? guess x)
  (< (abs (- (square guess) x)) 0.001))

(define (square x)
  (* x x))

(define (sqrt x)
  (sqrt-iter 1.0 x))

;; New if

(define (new-if predicate then-clause else-clause)
  (cond (predicate then-clause)
  (else else-clause)))

(sqrt 1000)

;; With the use of new-if, the program will loop forever, likely because it
;; always evaluates the else-clause, while the normal `if` function will stop
;; evaluation once the predicate is false.
