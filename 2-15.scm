#lang racket
(require (planet neil/sicp))

Yes she's right. We noticed in the 2-14 that dividing A by itself gave a larger
interval than before. So we always lose some amount of accuracy when evaluating
one of our values. Since par2 only explicitly uses R1 and R2 once each, whereas
par1 uses them twice each, par2 will introduce less uncertainty than par1.
