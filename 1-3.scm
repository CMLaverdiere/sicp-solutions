#lang racket
(require (planet neil/sicp))

(define (square x)
  (* x x))

(define (sum-of-squares a b)
  (+ (square a) (sqaure b)))

(define (sum-of-squares-of-two-largest a b c)
  (cond ((and (<= a b) (<= a c)) (sum-of-squares b c))
        ((and (<= b a) (<= b c)) (sum-of-squares a c))
        (else (sum-of-squares a b))))

(sum-of-squares-of-two-largest 3 2 5)
(sum-of-squares-of-two-largest 3 7 5)
(sum-of-squares-of-two-largest 8 7 5)
