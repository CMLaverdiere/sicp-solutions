#lang racket
(require (planet neil/sicp))

(define (cont-frac n d k)
  (define (index i)
    (cond ((= i k) (/ (n i) (d i)))
          ((= i 1) (/ (n i) (index (inc i))))
          (else (+ (d (dec i)) (/ (n i) (index (inc i)))))))
  (index 1))

(cont-frac (lambda (i) 1.0)
           (lambda (i)
             (if (= 2 (modulo i 3))
                 (* 2 (+ 1 (floor (/ i 3))))
                 1))
           20)

;; e = 2.71828
