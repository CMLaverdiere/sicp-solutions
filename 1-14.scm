#lang racket
(require (planet neil/sicp))

(+ (cc 11 4) (cc -39 5))
(+ (+ (cc 11 3) (cc -14 4)) 0)
(+ (+ (+ (cc 11 2) (cc 1 3)) 0) 0)
(+ (+ (+ (+ (cc 11 1) (cc 6 2)) (+ 0 1))))
(+ (+ (+ (+ 11 2) (+ 0 1))))
4

dime + penny
2 nickels + penny
nickel + 6 pennies
11 pennies

Growth can be represented by a binary tree with a depth of max(n, d), for cost n
with d denominations. Thus, exponential growth.

Space required is linear on n and d, as the stack grows at most to the depth of
the tree.
