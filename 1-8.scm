#lang racket
(require (planet neil/sicp))

;; Square root

(define (cube-root-iter guess x)
  (if (good-enough? guess x)
      guess
      (cube-root-iter (improve guess x)
                 x)))

(define (improve guess x)
  (/
   (+
    (/ x (square guess))
    (* 2 guess))
   3))

(define (good-enough? guess x)
  (< (abs (- (* guess guess guess) x)) 0.001))

(define (square x)
  (* x x))

(define (cube-root x)
  (cube-root-iter 1.0 x))

(cube-root 8)
(cube-root 343)
