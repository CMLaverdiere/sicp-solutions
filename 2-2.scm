#lang racket
(require (planet neil/sicp))

(define (make-segment start end)
  (cons start end))

(define (start-segment seg)
  (car seg))

(define (end-segment seg)
  (cdr seg))

(define (make-point x y)
  (cons x y))

(define (x-point point)
  (car point))

(define (y-point point)
  (cdr point))

(define (midpoint-segment seg)
  (average-of-points (start-segment seg) (end-segment seg)))

(define (average-of-points p1 p2)
  (make-point (/ (+ (x-point p1) (x-point p2)) 2)
              (/ (+ (y-point p1) (y-point p2)) 2)))

(define (print-point p)
  (newline)
  (display "(")
  (display (x-point p))
  (display ",")
  (display (y-point p))
  (display ")"))

(define v1 (make-point 1 2))
(define v2 (make-point -4 7))
(define seg (make-segment v1 v2))
(midpoint-segment seg) ;; Midpoint (-1.5, 4.5)
