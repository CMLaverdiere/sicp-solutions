#lang racket
(require (planet neil/sicp))

(define (make-center-width c w)
  (make-interval (- c w) (+ c w)))

(define (make-center-percent c tol)
  (make-center-width c (* c tol)))

(define (percent i)
  (/ (width i) (center i)))

(define (center i)
  (/ (+ (lower-bound i) (upper-bound i)) 2))

(define (width i)
  (/ (- (upper-bound i) (lower-bound i)) 2))

(define (make-interval a b) (cons a b))
(define (lower-bound i) (car i))
(define (upper-bound i) (cdr i))

(define (print-interval i)
  (display "[")
  (display (car i))
  (display ",")
  (display (cdr i))
  (display "]")
  (newline))

(print-interval (make-center-percent 5 .1))
