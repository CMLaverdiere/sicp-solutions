#lang racket
(require (planet neil/sicp))

(define (product f a b next)
  (if (> a b)
      1
      (* (f a) (product f (next a) b next))))

(define (product-iterative f a b next)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (* result (f a)))))
  (iter a 1))

(define (factorial n)
  (product identity 1 n inc))

(factorial 5)

(define (square x)
  (* x x))

;; This is a bit of a trick.
;; Note that each term is repeated twice, except for the 2 in the numerator. We
;; can then just square the sequence, excluding the 2. This leaves an extra
;; numerator term, which is just the accuracy, so we divide it out at the end.
(define (pi-over-4 accuracy)
  (define (add-two n) (+ n 2))
  (/ (* 2.0 (square (/ (product identity 4 accuracy add-two)
                       (product identity 3 (- accuracy 1) add-two))))
     accuracy))

(pi-over-4 5000)
(/ pi 4)
