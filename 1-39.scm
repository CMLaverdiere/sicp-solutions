#lang racket
(require (planet neil/sicp))

(define (square x)
  (* x x))

(define (cont-frac n d k)
  (define (index i)
    (cond ((= i k) (/ (n i) (d i)))
          ((= i 1) (/ (n i) (index (inc i))))
          (else (+ (d (dec i)) (/ (n i) (index (inc i)))))))
  (index 1))

(define (tan-cf x k)
  (cont-frac (lambda (i)
               (if (= i 1)
                   x
                   (- (square x)))) ;; Note the tricky negative here.
             (lambda (i)
               (- (* 2 i) 1))
             k))

(tan-cf (/ pi 4) 20)
(tan (/ pi 4))

(tan-cf (/ pi 3) 20)
(tan (/ pi 3))
