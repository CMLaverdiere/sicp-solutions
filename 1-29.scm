#lang racket
(require (planet neil/sicp))

(define (cube x) (* x x x))

(define (sum term a next b)
  (define (sum-index term a next b index)
    (if (> a b)
        0
        (+ (term a index)
           (sum-index term (next a) next b (+ index 1)))))
  (sum-index term a next b 0))

(define (integral f a b dx)
  (let* ((n (/ (- b a) dx))
         (h (/ (- b a) n)))
    (define (adj-f v index)
      (cond ((or (= index 0) (= index n)) (f v))
            ((even? index) (* 2 (f v)))
            (else (* 4 (f v)))))
    (* (/ h 3) (sum adj-f
                    (+ a h)
                    (lambda (a) (+ a h))
                    b))))

;; (define (integral f a b dx)
;;   (define (add-dx x) (+ x dx))
;;   (* (sum f (+ a (/ dx 2.0)) add-dx b)
;;      dx))

(integral cube 0 1 0.01)
(integral cube 0 1 0.001)

It's pretty accurate, but the original method was a bit more accurate.
