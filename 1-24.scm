#lang racket
(require (planet neil/sicp))

(define (square x)
  (* x x))

(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (remainder (square (expmod base (/ exp 2) m))
                    m))
        (else
         (remainder (* base (expmod base (- exp 1) m))
                    m))))

(define (fermat-test n)
  (define (try-it a)
    (= (expmod a n n) a))
  (try-it (+ 1 (random (- n 1)))))

(define (fast-prime? n times)
  (cond ((= times 0) true)
        ((fermat-test n) (fast-prime? n (- times 1)))
        (else false)))

(define (next n)
  (if (= n 2) 3
      (+ 2 n)))

(define (divides? a b)
  (= (remainder b a) 0))

(define (prime? n)
  (fast-prime? n 5))

(define (timed-prime-test n)
  (newline)
  (display n)
  (start-prime-test n (runtime)))

(define (start-prime-test n start-time)
  (if (prime? n)
      (report-prime (- (runtime) start-time))))

(define (report-prime elapsed-time)
  (display " *** ")
  (display elapsed-time))

(define (search-for-primes low high)
  (cond ((> low high) #f)
        ((even? low) (search-for-primes (+ low 1) high))
        ((even? high) (search-for-primes low (- high 1)))
        (else (timed-prime-test low)
              (search-for-primes (+ low 2) high))))

(search-for-primes 1000 1020) ;; 3 microsecond average.
(search-for-primes 10000 10040) ;; 16 microsecond average.
(search-for-primes 100000 100045) ;; 45 microsecond average.
(search-for-primes 1000000 1000040) ;; 150 microsecond average.

Using a test count of 5 for the fast-prime test, the primes near 1000 take a bit
longer to verify, but the primes near 1000000 take less than twice as long as
the smaller primes to compute, unlike the non-logarithmic version.
