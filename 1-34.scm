#lang racket
(require (planet neil/sicp))

(define (f g)
  (g 2))

(f square)

(f (lambda (z) (* z (+ z 1))))

;; (f f)
;; (f (f 2))
;; (f (2 2))
;; error, since 2 isn't a procedure.
