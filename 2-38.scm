#lang racket
(require (planet neil/sicp))

(define (fold-right fn initial seq)
  (if (null? seq) initial
      (fn (car seq) (fold-right fn initial (cdr seq)))))

(define (fold-left fn initial sequence)
  (define (iter result rest)
    (if (null? rest)
        result
        (iter (fn result (car rest))
              (cdr rest))))
  (iter initial sequence))

(fold-right / 1 (list 1 2 3)) ;; 3/2
(fold-left / 1 (list 1 2 3))  ;; 1/6
(fold-right list nil (list 1 2 3))
(fold-left list nil (list 1 2 3))

;; The fold function is commutative and associative iff foldr = foldl.
