#lang racket
(require (planet neil/sicp))

(define (double a) (* a 2))
(define (halve a) (/ a 2))

(define (fast-mul b n)
  (define (fast-mul-iter a b n)
    (cond ((= n 0) a)
          ((odd? n) (fast-mul-iter (+ a b) b (- n 1)))
          (else (fast-mul-iter (double a) b (halve n)))))
  (fast-mul-iter 0 b n))

(fast-mul 3 5)
(fast-mul 7 3)
