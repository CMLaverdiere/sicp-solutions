#lang racket
(require (planet neil/sicp))

(define (repeated f n)
  (define (iter m)
    (if (= m 1)
        f
        (compose f (iter (- m 1)))))
  (iter n))

(define (compose f g)
  (lambda (x) (f (g x))))

(define (square x)
  (* x x))

((repeated square 2) 5)
