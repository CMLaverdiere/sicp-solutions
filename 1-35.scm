#lang racket
(require (planet neil/sicp))

(define tolerance 0.00001)

(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))

(fixed-point (lambda (x) (+ 1.0 (/ 1 x))) 1)

;; Proof:
;; x = 1 + 1/x
;; => x - 1/x - 1 = 0
;; => x^2 - x - 1 = 0 (mult by x)
;; The roots of which are (by the quadratic formula)
;; (-b +- sqrt(b^2 - 4ac)) / 2a
;; (1 + sqrt(5)) / 2 and (1 + sqrt(5)) / 2
;; where the former is 1.618 = phi
