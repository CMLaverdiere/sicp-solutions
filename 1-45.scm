#lang racket
(require (planet neil/sicp))

;; I had a hell of a time with this one.

(define (repeated f n)
  (define (iter m)
    (if (= m 1)
        f
        (compose f (iter (- m 1)))))
  (iter n))

(define (compose f g)
  (lambda (x) (f (g x))))

(define (fixed-point f first-guess)
  (define tolerance 0.00001)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))

(define (average-damp f)
  (lambda (x) (average x (f x))))

(define (average x y) (/ (+ x y) 2))
(define (square x) (* x x))
(define (cube x) (* x x x))

(define (fast-expt b n)
  (define (fast-expt-iter a b n)
    (cond ((= n 0) a)
          ((odd? n) (fast-expt-iter (* a b) b (- n 1)))
          (else (fast-expt-iter (* a a) b (/ n 2)))))
  (fast-expt-iter 1 b n))

(define (sqrt x)
  (fixed-point (lambda (y) (average y (/ x y)))
               1.0))

(define (cube-root x)
  (fixed-point (lambda (y) (average y (/ x (square y))))
               1.0))

(define (fourth-root x)
  (fixed-point (lambda (y)
                 (average y (average y (/ x (cube y)))))
               1.0))

(define (nth-damp f n)
  ((repeated average-damp n) f))

(sqrt 25)
(cube-root 64)
(fourth-root 81)

(define (nth-root n x)
  (fixed-point (nth-damp (lambda (y) (/ x (fast-expt y (- n 1))))
                         (floor (/ (log n) (log 2))))
               1.0))

(nth-root 4 625)
(nth-root 16 2983483)

;; pattern
;; 2 (sq): 1
;; 3 (cb): 1
;; 4: 2
;; 8: 3
;; 16: 4

;; It looks like the nth-root requires lg(n) damping iterations.
