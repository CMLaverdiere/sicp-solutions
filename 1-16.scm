#lang racket
(require (planet neil/sicp))

(define (fast-expt b n)
  (define (fast-expt-iter a b n)
    (cond ((= n 0) a)
          ((odd? n) (fast-expt-iter (* a b) b (- n 1)))
          (else (fast-expt-iter (* a a) b (/ n 2)))))
  (fast-expt-iter 1 b n))

(fast-expt 3 5)
(fast-expt 7 3)
