#lang racket
(require (planet neil/sicp))

(define (accumulate fn initial seq)
  (if (null? seq) initial
      (fn (car seq) (accumulate fn initial (cdr seq)))))

(define (accumulate-n op init seqs)
  (if (null? (car seqs))
      nil
      (cons (accumulate op init (map car seqs))
            (accumulate-n op init (map cdr seqs)))))

(define (dot-product v w)
  (accumulate + 0 (map * v w)))

(define (matrix-*-vector m v)
  (map (lambda (row) (dot-product row v)) m))

(define (transpose m)
  (accumulate-n cons nil m))

(define (matrix-*-matrix m n)
  (let ((cols (transpose n)))
    (map (lambda (a) (matrix-*-vector cols a)) m)))

(dot-product '(1 2 3) '(-1 2 -2))
(matrix-*-vector '((1 2 3) (4 5 6) (7 8 9)) '(3 3 3))
(transpose '((1 2 3) (4 5 6) (7 8 9)))
(matrix-*-matrix '((-1 1) (1 -1)) '((2 2) (1 1)))
