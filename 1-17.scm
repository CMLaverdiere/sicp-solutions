#lang racket
(require (planet neil/sicp))

(define (double a) (* a 2))
(define (halve a) (/ a 2))

(define (fast-mul a b)
  (if (= 1 b) a
      (if (odd? b) (+ a (fast-mul a (- b 1)))
          (double (fast-mul a (halve b))))))

(fast-mul 3 5)
(fast-mul 63 20)
