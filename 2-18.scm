#lang racket
(require (planet neil/sicp))

(define (last-elem l)
  (if (null? (cdr l))
      (car l)
      (last-elem (cdr l))))

(define (head l)
  (if (null? (cdr l))
      nil
      (cons (car l) (head (cdr l)))))

(define (reverse l)
  (if (null? l)
      l
      (cons (last-elem l) (reverse (head l)))))

(last-elem (list 1 4 9 16 25))
(reverse (list 1 4 9 16 25))
(head (list 1 4 9 16 25))
