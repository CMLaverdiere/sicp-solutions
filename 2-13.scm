#lang racket
(require (planet neil/sicp))

(define (make-center-width c w)
  (make-interval (- c w) (+ c w)))

(define (make-center-percent c tol)
  (make-center-width c (* c tol)))

(define (percent i)
  (/ (width i) (center i)))

(define (center i)
  (/ (+ (lower-bound i) (upper-bound i)) 2))

(define (width i)
  (/ (- (upper-bound i) (lower-bound i)) 2))

(define (make-interval a b) (cons a b))
(define (lower-bound i) (car i))
(define (upper-bound i) (cdr i))

(define (print-interval i)
  (display "[")
  (display (car i))
  (display ",")
  (display (cdr i))
  (display "]")
  (newline))

(define (mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
        (p2 (* (upper-bound x) (lower-bound y)))
        (p3 (* (upper-bound x) (upper-bound y)))
        (p4 (* (lower-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
                   (max p1 p2 p3 p4))))

(define x (make-center-percent 9 .004))
(define y (make-center-percent 2 .003))
(percent (mul-interval x y)) ;; 0.007, the sum of tols.

(define a (make-center-percent 34 .34))
(define b (make-center-percent 62 .93))
(percent (mul-interval a b)) ;; 0.96, not the sum of tols.

;; It seems that the sum of the tolerances can be used as an approximation when
;; taking the product of small ones.

;; To prove this, let a and b be intervals with tolerances ta, tb, and centers ca, cb.
;; Then a = [ca - ca * ta, ca + ca * ta] = [ca(1 - ta), ca(1 + ta), and b analogous.
;; So (assuming all positive numbers), and ta, tb very small,
;; a * b = [ca(1-ta) * cb(1-tb), ca(1+ta) * cb(1+tb)]
;;       = [(ca)(cb)(1 - ta - tb + ta*tb), (ca)(cb)(1 + ta + tb + ta*tb)]
;; approx = [cc(1 - tc), cc(1 + tc)] for cc = ca * cb, tc = ta + tb.
;; (because ta*tb -> 0)
;; Thus, the product has a tolerance of the sum of the initial tolerances
;; if they are sufficiently small.
