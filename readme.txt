My solutions to exercises in the book Structure and Interpretation of Computer
Programs, written in Scheme (Racket). Not guaranteed to be correct or elegant.
