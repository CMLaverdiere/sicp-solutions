#lang racket
(require (planet neil/sicp))

(define (interval-width i)
  (/ (- (upper-bound i) (lower-bound i)) 2))

(define (width-of-sum i1-width i2-width)
  (+ i1-width i2-width))

(define (make-interval a b) (cons a b))
(define (lower-bound i) (car i))
(define (upper-bound i) (cdr i))

(define (add-interval x y)
  (make-interval (+ (lower-bound x) (lower-bound y))
                 (+ (upper-bound x) (upper-bound y))))

(define (mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
        (p2 (* (upper-bound x) (lower-bound y)))
        (p3 (* (upper-bound x) (upper-bound y)))
        (p4 (* (lower-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
                   (max p1 p2 p3 p4))))

(define (print-interval i)
  (display "[")
  (display (car i))
  (display ",")
  (display (cdr i))
  (display "]")
  (newline))

(define x (make-interval 1 12)) ;; width 11/2
(define y (make-interval -3 9)) ;; width 6
(width-of-sum (interval-width x) (interval-width y))
(interval-width (add-interval x y)) ;; width 23/2

;; The same pattern doesn't apply to mul/div.
(define (width-of-prod i1-width i2-width)
  (* i1-width i2-width))

(width-of-prod (interval-width x) (interval-width y)) ;; 33
(interval-width (mul-interval x y)) ;; 72 != 33
