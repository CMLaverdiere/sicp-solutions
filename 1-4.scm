#lang racket
(require (planet neil/sicp))

;; The operator depends on the result of the if expression.
(define (a-plus-abs-b a b)
  ((if (> b 0) + -) a b))

(a-plus-abs-b 5 (- 2))
