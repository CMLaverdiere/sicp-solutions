#lang racket
(require (planet neil/sicp))

;; Square root

(define (sqrt-iter guess x)
  (if (good-enough? guess x)
      guess
      (sqrt-iter (improve guess x)
                 x)))

(define (improve guess x)
  (average guess (/ x guess)))

(define (average x y)
  (/ (+ x y) 2))

(define (good-enough? guess x)
  (< (abs (- (square guess) x)) 0.001))

(define (square x)
  (* x x))

(define (sqrt x)
  (sqrt-iter 1.0 x))

;; I couldn't find any large numbers that don't work.
;; This gives 0.031, certainly too large for a square root.
(sqrt 1e-12)

;; New square root method.

(define (change-small-enough? new-guess old-guess)
  (< (/ (abs (- new-guess old-guess)) new-guess) 0.001))

(define (new-sqrt-iter guess x)
  (define new-guess (improve guess x))
  (if (change-small-enough? new-guess guess)
      guess
      (new-sqrt-iter new-guess x)))

(define (new-sqrt x)
  (new-sqrt-iter 1.0 x))

;; This gives 1.00045e-06, much better.
(new-sqrt 1e-12)
