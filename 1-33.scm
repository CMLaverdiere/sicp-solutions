#lang racket
(require (planet neil/sicp))

(define (filtered-accumulate combiner filter null-value term a next b)
  (if (> a b)
      null-value
      (combiner (if (filter a)
                    (term a)
                    null-value)
                (filtered-accumulate combiner filter null-value term (next a) next b))))

(define (square x)
  (* x x))

(define (smallest-divisor n)
  (find-divisor n 2))

(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (+ test-divisor 1)))))

(define (divides? a b)
  (= (remainder b a) 0))

(define (prime? n)
  (= (smallest-divisor n) n))

(define (gcd m n)
  (if (= n 0)
      m
      (gcd n (modulo m n))))

(define (sum-prime-squares a b)
  (filtered-accumulate + prime? 0 square a inc b))

(define (product-coprime n)
  (define (coprime-with-n? m)
    (= (gcd n m) 1))
  (filtered-accumulate * coprime-with-n? 1 identity 2 inc (- n 1)))

(sum-prime-squares 5 11) ;; 5^2 + 7^2 + 11^2 = 195
(product-coprime 8) ;; 3 * 5 * 7 = 105
