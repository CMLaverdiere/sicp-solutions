#lang racket
(require (planet neil/sicp))

(define (cube x) (* x x x))

(define (p x) (- (* 3 x) (* 4 (cube x))))

(define (sine angle)
  (if (not (> (abs angle) 0.1))
      angle
      (p (sine (/ angle 3.0)))))

(sine 500)

a. p is applied 5 times.

b. The space required is how many times we recurse, which is log_3 of the angle,
plus some constant factor for the lower tolerance. The number of steps is
2*space, since p executes each time sine executes, except in the base case.
