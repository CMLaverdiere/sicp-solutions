#lang racket
(require (planet neil/sicp))

(define (accumulate fn initial seq)
  (if (null? seq) initial
      (fn (car seq) (accumulate fn initial (cdr seq)))))

(define (my-map p sequence)
  (accumulate (lambda (x y) (cons (p x) y)) nil sequence))

(define (my-append seq1 seq2)
  (accumulate cons seq2 seq1))

(define (my-length seq)
  (accumulate (lambda (x y) (+ 1 y)) 0 seq))
