#lang racket
(require (planet neil/sicp))

(define (square-list items)
  (define (iter things answer)
    (if (null? things)
        answer
        (iter (cdr things)
              (cons (square (car things))
                    answer))))
    (iter items nil))

;; This returns the list backwards since the car from the first step is at the
;; furthest cons spot.
(square-list '(1 2 3 4))

(define (square-list items)
  (define (iter things answer)
    (if (null? things)
        answer
        (iter (cdr things)
              (cons answer
                    (square (car things))))))
    (iter items nil))

;; Louis still hasn't changed the order that the items are being cons'd.
(square-list '(1 2 3 4))
