#lang racket
(require (planet neil/sicp))

The interpreter will evaluate the expmod expression twice on each iteration.
This creates an expression tree logarithmic depth. Rather than evaluating
O(lg(n)) expressions, we evaluate O(2^lg(n)) = O(n) expressions.
