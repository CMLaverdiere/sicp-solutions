#lang racket
(require (planet neil/sicp))

(define tolerance 0.00001)

(define (iterative-improve good-enough? improve)
  (lambda (first-guess)
    (define (try guess)
      (if (good-enough? guess)
          guess
          (try (improve guess))))
    (try first-guess)))

(define (sqrt x)
  (define (close-enough? guess)
    (< (abs (- (square guess) x)) tolerance))
  (define (improve guess)
    (average guess (/ x guess)))
  ((iterative-improve close-enough? improve) x))

(define (average x y)
  (/ (+ x y) 2))

(define (square x)
  (* x x))

(sqrt 9.0)
(sqrt 37.0)

(define (fixed-point f first-guess)
  (define (close-enough? guess)
    (< (abs (- guess (f guess))) tolerance))
  (define (improve guess)
    (f guess))
  ((iterative-improve close-enough? improve) first-guess))

(fixed-point (lambda (x) (/ (log 1000) (log x))) 5)
