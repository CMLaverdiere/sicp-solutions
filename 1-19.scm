#lang racket
(require (planet neil/sicp))

;; If T_pq(a) = bq + aq + ap, and
;;    T_pq(b) = bp + aq, then
;;
;; T_pq(T_pq(a)) = T_pq(bq + aq + ap)
;;               = (bp + aq)q + (bq + aq + ap)q + (bq + aq + ap)p
;;               = bpq + aq^2 + bq^2 + aq^2 + apq + bpq + apq + ap^2
;;               = 2bpq + 2apq + 2aq^2 + bq^2 + ap^2
;;               = b(2pq + q^2) + a(2pq + q^2) + a(q^2 + p^2)
;;               = bq' + aq' + ap'
;;               = T_p'q'(a)
;;
;; T_pq(T_pq(b)) = (bp + aq)p + (bq + aq + ap)q
;;               = bp^2 + aqp + bq^2 + aq^2 + apq
;;               = 2aqp + bp^2 + bq^2 + aq^2
;;               = b(p^2 + q^2) + a(2qp + q^2)
;;               = bp' + aq'
;;               = T_p'q'(b)
;;
;; Thus, p' = p^2 + q^2, and q' = 2qp + q^2.

(define (fib n)
  (fib-iter 1 0 0 1 n))

(define (fib-iter a b p q count)
  (cond ((= count 0) b)
        ((even? count)
         (fib-iter a
                   b
                   (+ (* p p) (* q q))
                   (+ (* 2 q p) (* q q))
                   (/ count 2)))
        (else (fib-iter (+ (* b q) (* a q) (* a p))
                        (+ (* b p) (* a q))
                        p
                        q
                        (- count 1)))))

(fib 1)
(fib 2)
(fib 3)
(fib 4)
(fib 5)
(fib 6)
(fib 7)
(fib 8)
