#lang racket
(require (planet neil/sicp))

(define (next n)
  (if (= n 2) 3
      (+ 2 n)))

(define (square x)
  (* x x))

(define (smallest-divisor n)
  (find-divisor n 2))

(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (next test-divisor)))))

(define (divides? a b)
  (= (remainder b a) 0))

(define (prime? n)
  (= (smallest-divisor n) n))

(define (timed-prime-test n)
  (newline)
  (display n)
  (start-prime-test n (runtime)))

(define (start-prime-test n start-time)
  (if (prime? n)
      (report-prime (- (runtime) start-time))))

(define (report-prime elapsed-time)
  (display " *** ")
  (display elapsed-time))

(define (search-for-primes low high)
  (cond ((> low high) #f)
        ((even? low) (search-for-primes (+ low 1) high))
        ((even? high) (search-for-primes low (- high 1)))
        (else (timed-prime-test low)
              (search-for-primes (+ low 2) high))))

(search-for-primes 1000 1020) ;; 3 microsecond average.
(search-for-primes 10000 10040) ;; 16 microsecond average.
(search-for-primes 100000 100045) ;; 45 microsecond average.
(search-for-primes 1000000 1000040) ;; 150 microsecond average.

It is around 1/2 to 2/3 the original time, as expected, since `next` branches.
