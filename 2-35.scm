#lang racket
(require (planet neil/sicp))

(define (accumulate fn initial seq)
  (if (null? seq) initial
      (fn (car seq) (accumulate fn initial (cdr seq)))))

(define (my-map p sequence)
  (accumulate (lambda (x y) (cons (p x) y)) nil sequence))

(define (count-leaves t)
  (accumulate + 0 (my-map
                   (lambda (x) (if (pair? x) (count-leaves x) 1))
                   t)))

(count-leaves (cons (list 1 2) (list 3 4)))
