#lang racket
(require (planet neil/sicp))

;; Rows / columns indexed starting at 1.
(define (pascal row col)
  (if (or (= col 1) (= col row))
      1
      (+ (pascal (- row 1) (- col 1)) (pascal (- row 1) col))))

(pascal 5 3)
(pascal 6 5)
