#lang racket
(require (planet neil/sicp))

(define (accumulate fn initial seq)
  (if (null? seq) initial
      (fn (car seq) (accumulate fn initial (cdr seq)))))

(define (accumulate-n op init seqs)
  (if (null? (car seqs))
      nil
      (cons (accumulate op init (map car seqs))
            (accumulate-n op init (map cdr seqs)))))

(define s '((1 2 3) (4 5 6) (7 8 9) (10 11 12)))
(accumulate-n + 0 s)
