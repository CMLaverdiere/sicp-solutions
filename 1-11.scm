#lang racket
(require (planet neil/sicp))

; Recursive process
(define (f n)
  (if (< n 3) n
      (+ (f (- n 1)) (* 2 (f (- n 2))) (* 3 (f (- n 3))))))

;; Iterative process
(define (g n)
  (define (g-iter a b c n)
    (if (= n 1) a
        (g-iter b c (+ (* 3 a) (* 2 b) c) (- n 1))))
  (if (= n 0) 0
      (g-iter 1 2 4 n)))

(f 6)
(g 6)
