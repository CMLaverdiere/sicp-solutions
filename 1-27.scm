#lang racket
(require (planet neil/sicp))

(define (square x)
  (* x x))

(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (remainder (square (expmod base (/ exp 2) m))
                    m))
        (else
         (remainder (* base (expmod base (- exp 1) m))
                    m))))

(define (fermat-test-all n c)
  (define (passes-fermat-test a)
    (= (expmod a n n) a))
  (cond ((< c 0) #t)
        ((passes-fermat-test c) (fermat-test-all n (- c 1)))
        (else #f)))

(define (prime? n)
  (fermat-test-all n (- n 1)))

;; These all report true, but none are prime (Carmichael numbers).
(prime? 561)
(prime? 1105)
(prime? 1729)
(prime? 2465)
(prime? 2821)
(prime? 6601)
