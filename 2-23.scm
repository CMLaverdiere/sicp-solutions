#lang racket
(require (planet neil/sicp))

(define (for-each f l)
  (f (car l))
  (if (not (null? (cdr l)))
      (for-each f (cdr l))))

(for-each (lambda (x) (newline) (display x))
          (list 57 321 88))
