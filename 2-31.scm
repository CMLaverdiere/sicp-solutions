#lang racket
(require (planet neil/sicp))

(define (tree-map fn tree)
  (map (lambda (subtree)
         (if (pair? subtree)
             (tree-map fn subtree)
             (fn subtree)))
       tree))

(define (square-tree tree) (tree-map square tree))

(define (square x)
  (* x x))

(square-tree
 (list 1
       (list 2 (list 3 4) 5)
       (list 6 7)))
