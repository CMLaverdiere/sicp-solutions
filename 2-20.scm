#lang racket
(require (planet neil/sicp))

(define (same-parity x . rest)
  (define (iter left)
    (if (null? left)
        left
        (if (even? (- x (car left)))
            (cons (car left) (iter (cdr left)))
            (iter (cdr left)))))
  (cons x (iter rest)))

(same-parity 1 2 3 4 5 6 7)
(same-parity 2 3 4 5 6 7)
