#lang racket
(require (planet neil/sicp))

(define (smooth f)
  (define dx 0.0001)
  (lambda (x) (average-3 (f (- x dx))
                         (f x)
                         (f (+ x dx)))))

(define (repeated-smooth f n)
  ((repeated smooth n) f))

(define (repeated f n)
  (define (iter m)
    (if (= m 1)
        f
        (compose f (iter (- m 1)))))
  (iter n))

(define (compose f g)
  (lambda (x) (f (g x))))

(define (average-3 a b c)
  (/ (+ a b c) 3))

(define (jaggy x)
  (- 1 (abs x)))

;; The function is an upside-down V at x=0. Smoothing out the tip makes f(0)
;; slighly less than 1, as expected.
(jaggy 0)
((repeated-smooth jaggy 2) 0)
((repeated-smooth jaggy 5) 0)
((repeated-smooth jaggy 15) 0)

;;  1 +-+---------------+----------------**----------------+---------------+-+
;;    +                 +              ** +**              +                 +
;;  0 +-+                            **      **               jaggy  *******-+
;;    |                            **          **                            |
;; -1 +-+                        ***            ***                        +-+
;;    |                         **                **                         |
;; -2 +-+                     **                    **                     +-+
;;    |                     **                        **                     |
;; -3 +-+                 **                            **                 +-+
;; -4 +-+               **                                **               +-+
;;    |               **                                    **               |
;; -5 +-+           ***                                      ***           +-+
;;    |           ***                                          ***           |
;; -6 +-+        *                                                *        +-+
;;    |        **                                                  **        |
;; -7 +-+    **                                                      **    +-+
;;    |    **                                                          **    |
;; -8 +-+**                                                              **+-+
;;    +**               +                 +                +               **+
;; -9 **+---------------+-----------------+----------------+---------------+**
;;   -10               -5                 0                5                 10
