#lang racket
(require (planet neil/sicp))

(define (square x)
  (* x x))

(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (let ((sq (square (expmod base (/ exp 2) m))))
           (if (and (= sq 1) (not (or (= exp 1) (= exp (- m 1)))))
               0
               (remainder sq m))))
        (else
         (remainder (* base (expmod base (- exp 1) m))
                    m))))

(define (fermat-test n)
  (define (try-it a)
    (= (expmod a n n) a))
  (try-it (+ 1 (random (- n 1)))))

(define (fast-prime? n times)
  (cond ((= times 0) true)
        ((fermat-test n) (fast-prime? n (- times 1)))
        (else false)))

(define (next n)
  (if (= n 2) 3
      (+ 2 n)))

(define (divides? a b)
  (= (remainder b a) 0))

(define (prime? n)
  (fast-prime? n 50))

;; Carmichael numbers.
;; Under the Miller-Rabin test, these are found to be pseudoprimes.
;; At least, most of the time. The tests are randomized afterall.
(prime? 561)
(prime? 1105)
(prime? 1729)
(prime? 2465)
(prime? 2821)
(prime? 6601)
