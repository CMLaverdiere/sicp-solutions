#lang racket
(require (planet neil/sicp))

(define (fringe tree)
  (if (null? tree)
      nil
      (if (list? tree)
          (append (fringe (car tree)) (fringe (cdr tree)))
          (list tree))))

(define x (list (list 1 2) (list 3 4)))
(fringe x)
(fringe (list x x))
