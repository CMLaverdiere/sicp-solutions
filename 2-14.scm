#lang racket
(require (planet neil/sicp))

(define (par1 r1 r2)
  (div-interval (mul-interval r1 r2)
                (add-interval r1 r2)))

(define (par2 r1 r2)
  (let ((one (make-interval 1 1)))
    (div-interval one
                  (add-interval (div-interval one r1)
                                (div-interval one r2)))))

(define (add-interval x y)
  (make-interval (+ (lower-bound x) (lower-bound y))
                 (+ (upper-bound x) (upper-bound y))))

(define (mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
        (p2 (* (upper-bound x) (lower-bound y)))
        (p3 (* (upper-bound x) (upper-bound y)))
        (p4 (* (lower-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
                   (max p1 p2 p3 p4))))

(define (div-interval x y)
  (if (and (> (upper-bound y) 0) (< (lower-bound y) 0))
      (error "Interval spans zero")
      (mul-interval x
                    (make-interval (/ 1.0 (upper-bound y))
                                   (/ 1.0 (lower-bound y))))))

(define (make-center-percent c tol)
  (make-center-width c (* c tol)))

(define (make-interval a b) (cons a b))
(define (lower-bound i) (car i))
(define (upper-bound i) (cdr i))

(define (print-interval i)
  (display "[")
  (display (car i))
  (display ",")
  (display (cdr i))
  (display "]")
  (newline))

;; Lem is right.
(define x (make-interval 3.2 9.5))
(define y (make-interval 17.6 3.8))
(par1 x y)
(par2 x y)

(par1 (make-center-percent 52 0.5) (make-center-percent 207 0.7))
(par2 (make-center-percent 52 0.5) (make-center-percent 207 0.7))

(par1 (make-center-percent 52 0.1) (make-center-percent 207 0.2))
(par2 (make-center-percent 52 0.1) (make-center-percent 207 0.2))

(par1 (make-center-percent 52 0.01) (make-center-percent 207 0.02))
(par2 (make-center-percent 52 0.01) (make-center-percent 207 0.02))

;; The smaller the tolerance, the less the error.

(define a (make-center-percent 100 0.01))
(define b (make-center-percent 30 0.02))

(div-interval a a)
(div-interval a b)
