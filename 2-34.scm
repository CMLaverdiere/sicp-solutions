#lang racket
(require (planet neil/sicp))

(define (accumulate fn initial seq)
  (if (null? seq) initial
      (fn (car seq) (accumulate fn initial (cdr seq)))))

(define (horner-eval x coefficient-sequence)
  (accumulate (lambda (this-coeff higher-terms) (+ this-coeff (* x higher-terms)))
              0
              coefficient-sequence))

;; f(x) = 1 + 3x + 5x^3 + x^5 at f(2) = 79
(horner-eval 2 (list 1 3 0 5 0 1))
