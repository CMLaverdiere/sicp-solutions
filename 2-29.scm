#lang racket
(require (planet neil/sicp))

(define (make-mobile left right)
  (list left right))

(define (make-branch length structure)
  (list length structure))

(define (left-branch mobile)
  (car mobile))

(define (right-branch mobile)
  (cadr mobile))

(define (branch-length branch)
  (car branch))

(define (branch-structure branch)
  (cadr branch))

(define (total-weight mobile)
  (+ (total-branch-weight (left-branch mobile))
     (total-branch-weight (right-branch mobile))))

(define (total-branch-weight branch)
  (+ (branch-length branch)
     (let ((structure (branch-structure branch)))
       (if (pair? structure)
           (total-weight structure)
           structure))))

(define (torque branch)
  (* (branch-length branch)
     (let ((structure (branch-structure branch)))
       (if (pair? structure)
           (total-weight structure)
           structure))))

(define (mobile-balanced mobile)
  (if (pair? mobile)
      (and
       (= (torque (left-branch mobile))
           (torque (right-branch mobile)))
       (mobile-balanced (branch-structure (left-branch mobile)))
       (mobile-balanced (branch-structure (right-branch mobile))))
      true))

(define mobile
  (make-mobile
   (make-branch 5 2)
   (make-branch
    4
    (make-mobile (make-branch 6 7) (make-branch 1 2)))))

(define mobile-bal
  (make-mobile
   (make-branch 2 4)
   (make-branch 1 (make-mobile (make-branch 2 2) (make-branch 2 2)))))

;; Tests:
(total-weight mobile)
(mobile-balanced mobile)
(mobile-balanced mobile-bal)

;; If we change the representation to use cons rather than lists, we need only
;; change 'cadr' to 'cdr' in the left/right-branch functions.
