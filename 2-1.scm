#lang racket
(require (planet neil/sicp))

(define (make-rat n d)
  (let ((g (gcd n d))
        (n-signed
         (if (< d 0)
             (- n)
             n)))
        (cons (/ n-signed g) (/ (abs d) g))))

(define (numer x) (car x))
(define (denom x) (cdr x))

(define (add-rat x y)
  (make-rat (+ (* (numer x) (denom y))
               (* (numer y) (denom x)))
            (* (denom x) (denom y))))

(define (sub-rat x y)
  (make-rat (- (* (numer x) (denom y))
               (* (numer y) (denom x)))
            (* (denom x) (denom y))))

(define (mul-rat x y)
  (make-rat (* (numer x) (numer y))
            (* (denom x) (denom y))))

(define (div-rat x y)
  (make-rat (* (numer x) (denom y))
            (* (denom x) (numer y))))

(define (equal-rat? x y)
  (make-rat (= (* (numer x) (denom y))
               (* (denom x) (numer y)))))

(define (print-rat x)
  (display (numer x))
  (display "/")
  (display (denom x))
  (newline))

(define one-half (make-rat 1 2))
(define one-third (make-rat 1 3))
(define neg-one-half (make-rat 1 -2))
(define one-fourth (make-rat -1 -4))

(print-rat (add-rat one-half one-third))
(print-rat (mul-rat one-half one-third))
(print-rat (add-rat one-third one-third))

(print-rat neg-one-half)
(print-rat (mul-rat neg-one-half one-third))
(print-rat (mul-rat neg-one-half neg-one-half))
(print-rat (div-rat one-half neg-one-half))

(print-rat (sub-rat one-fourth neg-one-half))
(print-rat (sub-rat one-fourth one-half))
