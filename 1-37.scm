#lang racket
(require (planet neil/sicp))

;; Recursive

(define (cont-frac n d k)
  (define (index i)
    (cond ((= i k) (/ (n i) (d i)))
          ((= i 1) (/ (n i) (index (inc i))))
          (else (+ (d (dec i)) (/ (n i) (index (inc i)))))))
  (index 1))

(cont-frac (lambda (i) 1.0)
           (lambda (i) 1.0)
           14)

;; Iterative

(define (cont-frac-iter n d k)
  (define (iter i result)
    (if (= i 1)
        (/ (n 1) result)
        (iter (dec i) (+ (d (dec i)) (/ (n i) result)))))
  (iter k (d k)))

(cont-frac-iter (lambda (i) 1.0)
                (lambda (i) 1.0)
                14)

;; phi = 1.61803, so it takes around 14 steps for this accuracy.
