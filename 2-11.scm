#lang racket
(require (planet neil/sicp))

;; (define (mul-interval x y)
;;   (let ((p1 (* (lower-bound x) (lower-bound y)))
;;         (p2 (* (upper-bound x) (lower-bound y)))
;;         (p3 (* (upper-bound x) (upper-bound y)))
;;         (p4 (* (lower-bound x) (upper-bound y))))
;;     (make-interval (min p1 p2 p3 p4)
;;                    (max p1 p2 p3 p4))))

;; If you reversed the < and >=s on the 0 comps like I did, here's a regex:
;; '<,'>s/\([<>]=?\) 0 \([lu]\)b\([xy]\)/\1 \2b\3 0
(define (mul-interval x y)
  (let ((lbx (lower-bound x))
        (lby (lower-bound y))
        (ubx (upper-bound x))
        (uby (upper-bound y)))
    (cond
     ((and (>= lbx 0) (>= lby 0) (>= ubx 0) (>= uby 0))
      (make-interval (* lbx lby) (* ubx uby)))
     ((and (>= lbx 0) (< lby 0) (>= ubx 0) (>= uby 0))
      (make-interval (* ubx lby) (* ubx uby)))
     ((and (< lbx 0) (>= lby 0) (>= ubx 0) (>= uby 0))
      (make-interval (* lbx uby) (* ubx uby)))
     ((and (< lbx 0) (< lby 0) (>= ubx 0) (>= uby 0))
      (make-interval (min (* lbx uby) (* ubx lby))
                     (max (* lbx lby) (* ubx lby))))
     ((and (>= lbx 0) (< lby 0) (>= ubx 0) (< uby 0))
      (make-interval (* ubx lby) (* lbx uby)))
     ((and (< lbx 0) (< lby 0) (>= ubx 0) (< uby 0))
      (make-interval (* ubx lby) (* lbx lby)))
     ((and (< lbx 0) (>= lby 0) (< ubx 0) (>= uby 0))
      (make-interval (* lbx uby) (* ubx lby)))
     ((and (< lbx 0) (< lby 0) (< ubx 0) (>= uby 0))
      (make-interval (* lbx uby) (* lbx lby)))
     ((and (< lbx 0) (< lby 0) (< ubx 0) (< uby 0))
      (make-interval (* ubx uby) (* lbx lby)))
     (else
      (error "Invalid interval argument order")))))

     ;; 0 negative, 1 positive.
     ;; There are 9 possible (valid) cases, one requiring 4 muls.
     ;; | ubx | uby | lbx | lby | [min, max]
     ;; |   0 |   0 |   0 |   0 | [ubx*uby, lbx*lby]
     ;; |   0 |   0 |   0 |   1 | Invalid
     ;; |   0 |   0 |   1 |   0 | Invalid
     ;; |   0 |   0 |   1 |   1 | Invalid
     ;; |   0 |   1 |   0 |   0 | [lbx*uby, lbx*lby]
     ;; |   0 |   1 |   0 |   1 | [lbx*uby, ubx*lby]
     ;; |   0 |   1 |   1 |   0 | Invalid
     ;; |   0 |   1 |   1 |   1 | Invalid
     ;; |   1 |   0 |   0 |   0 | [ubx*lby, lbx*lby]
     ;; |   1 |   0 |   0 |   1 | Invalid
     ;; |   1 |   0 |   1 |   0 | [ubx*lby, lbx*uby]
     ;; |   1 |   0 |   1 |   1 | Invalid
     ;; |   1 |   1 |   0 |   0 | [lbx*uby or ubx*lby, lbx*lby or ubx*uby]
     ;; |   1 |   1 |   0 |   1 | [lbx*uby, ubx*uby]
     ;; |   1 |   1 |   1 |   0 | [ubx*lby, ubx*uby]
     ;; |   1 |   1 |   1 |   1 | [lbx*lby, ubx*uby]

(define (make-interval a b) (cons a b))
(define (lower-bound i) (car i))
(define (upper-bound i) (cdr i))

(define (print-interval i)
  (display "[")
  (display (car i))
  (display ",")
  (display (cdr i))
  (display "]")
  (newline))

;; This is all the testing I'm going to do. The main idea is there.
(define x (make-interval 5 7))
(define y (make-interval -3 4))

(print-interval (mul-interval x y))
